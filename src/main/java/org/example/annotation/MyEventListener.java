package org.example.annotation;

import org.example.EventDemo;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MyEventListener {

    @EventListener(condition = "#myEvent.message!='test'") ///以使用SPEL表达式来过滤监听到事件，即只有符合某种条件的才进行接收处理
    @Order(2)
    @Async
    public void handleEvent(MyEvent myEvent) {
        System.out.println("handleEvent1===========");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("handleEvent1:" + myEvent.getMessage());
    }


    @EventListener
    @Order(1) //可以在方法上使用spring的@order注解来定义多个监听器的顺序
    @Async
    public void handleEvent2(MyEvent myEvent) {
        System.out.println("handleEvent2===========");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("handleEvent2:" +myEvent.getMessage());
    }


    @EventListener(classes = {MyEvent.class, EventDemo.class})
    @Async
    @Order(3)
    public void handleEvent3(Object object) {
        //如果要监听多个事件类型的发布，可以在@EventListener(classes = {XXX.class,YYY.class})指定
        // spring会多次调用此方法来处理多个事件。
        // 但是注意此时，方法参数不能有多个，否则会发生转换异常，可以将使用多个事件的父类作为唯一的方法参数来接收处理事件，
        // 但除非必要否则并不推荐监听多个事件的发布。
        System.out.println("handleEvent3===========");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (object instanceof MyEvent) {
            System.out.println("handleEvent3:" +((MyEvent)object).getMessage());
        }
        if (object instanceof EventDemo) {
            System.out.println("handleEvent3:" +((EventDemo)object).getMessage());
        }
    }



}
