package org.example;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Publisher {

    private List<Subscriber> subscribers = new ArrayList<>();

    private int state;

    public void attach(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void notifyAllSubscribers() {
        for (Subscriber subscriber : subscribers) {
            subscriber.update();
        }
    }

    public void setState(int state) {
        this.state = state;
        notifyAllSubscribers();
    }
}
