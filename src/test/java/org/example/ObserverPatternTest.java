package org.example;

import org.junit.jupiter.api.Test;

public class ObserverPatternTest {


    @Test
    void testObserverPattern() {
        Publisher publisher = new Publisher();

        SubscriberA subscriberA = new SubscriberA(publisher);
        SubscriberB subscriberB = new SubscriberB(publisher);

        publisher.setState(15);
        System.out.println("=========");
        publisher.setState(10);
    }
}
