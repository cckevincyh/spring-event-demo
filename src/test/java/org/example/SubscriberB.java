package org.example;

public class SubscriberB extends Subscriber {
    public SubscriberB(Publisher publisher) {
        super(publisher);
        publisher.attach(this);
    }

    @Override
    public void update() {
        System.out.println("SubscriberB update..." + this.getPublisher().getState());
    }
}
