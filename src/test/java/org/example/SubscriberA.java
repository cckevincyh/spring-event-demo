package org.example;

public class SubscriberA extends Subscriber{
    public SubscriberA(Publisher publisher) {
        super(publisher);
        publisher.attach(this);
    }

    @Override
    public void update() {
        System.out.println("SubscriberA update..." + this.getPublisher().getState());
    }
}
