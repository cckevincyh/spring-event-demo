package org.example;

import org.example.annotation.MyEvent;
import org.example.annotation.MyEventPublisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MyEventPublishTest {

    @Autowired
    private MyEventPublisher myEventPublisher;


    @Test
    void testEventDemoPublish() throws InterruptedException {
        myEventPublisher.publish(new MyEvent("test"));
        myEventPublisher.publish(new MyEvent("test1"));
        Thread.sleep(5000);
    }
}
