package org.example;

import lombok.Data;

@Data
public abstract class Subscriber {
    private final Publisher publisher;

    public Subscriber(Publisher publisher) {
        this.publisher = publisher;
    }

    public abstract void update();
}
