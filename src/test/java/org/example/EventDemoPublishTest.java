package org.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EventDemoPublishTest {

    @Autowired
    private EventDemoPublish eventDemoPublish;


    @Test
    void testEventDemoPublish() throws InterruptedException {
        eventDemoPublish.publish("test message");
        Thread.sleep(5000);
    }
}
